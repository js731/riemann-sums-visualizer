from math import *
import numpy as np
import scipy.integrate as integrate
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
from matplotlib.widgets import TextBox

RESOLUTION = 1000
RECT_MIN = 1
RECT_NUM = 15
RECT_MAX = 100
START = 1
STOP = 10
FUNCTION = "(2000.0 / x) + pow(x, 4) * sin(x)"

def delta_x(n):
	return (STOP - START) / float(n)

def function(n, Dx, start):
	z = []
	for i in range(0, n + 1):
		z.append(integrand((i * Dx) + start))
	return z

def LHS(Dx, points):
	s = 0
	for i in points[:-1]:
		s = s + i * Dx
	return s

def RHS(Dx, points):
	s = 0
	for i in points[1:]:
		s = s + i * Dx
	return s

def TR(Dx, points):
	return (0.5) * (RHS(Dx, points) + LHS(Dx, points))

def MPS(Dx):
	z = function(RECT_NUM, Dx, START - (Dx / 2.0))
	s = 0
	for q in z[1:]:
		s = s + (q * Dx)
	return s

def integrand(x):
	#https://www.geeksforgeeks.org/eval-in-python/
	safe_list = ['acos', 'asin', 'atan', 'atan2', 'ceil', 'cos', 
		  'cosh', 'degrees', 'e', 'exp', 'fabs', 'floor', 
		  'fmod', 'frexp', 'hypot', 'ldexp', 'log', 'log10', 
		  'modf', 'pi', 'pow', 'radians', 'sin', 'sinh', 'sqrt', 
		  'tan', 'tanh'] 
	safe_dict = dict([(k, globals().get(k, None)) for k in safe_list])
	safe_dict['x'] = x
	y = eval(FUNCTION, {"__builtins__" : None}, safe_dict)
	return y

def integration():
	return integrate.quad(integrand, START, STOP)

def draw():
	DxFunct = delta_x(RESOLUTION) # used for plotting integrand
	functXPoints = []

	for i in range(RESOLUTION + 1):	#x coordinates for full graph into functXPoints
		functXPoints.append((i * DxFunct) + START)

	fig = plt.figure(1)
	
	slider_ax = plt.axes([0.11, 0.03, 0.35, 0.03])
	text_ax = plt.axes([0.56, 0.03, 0.35, 0.03])
	rect_slider = Slider(slider_ax, 'Rectangles', RECT_MIN, RECT_MAX, valinit = RECT_NUM, valstep = 1)
	text_box = TextBox(text_ax, 'Function', initial=FUNCTION)

	lines = []

	ax1 = plt.subplot(2, 2, 1)
	ax2 = plt.subplot(2, 2, 2)
	ax3 = plt.subplot(2, 2, 3)
	ax4 = plt.subplot(2, 2, 4)	


	def submit(eq):
		global FUNCTION

		FUNCTION = eq
		
		functYPoints = function(RESOLUTION, DxFunct, START) #used for plotting integrand
		
		plt.suptitle(r'Riemann Sums for $f(x) = ' + FUNCTION + '$ (Integral = ' + str(round(integration()[0], 5)) + ')', fontsize = 18)
		
		ax1.clear()
		plt.axes(ax1)
		plt.plot(functXPoints, functYPoints)
		plt.hlines(0, START, STOP)

		ax2.clear()
		plt.axes(ax2)
		plt.plot(functXPoints, functYPoints)
		plt.hlines(0, START, STOP)
		
		ax3.clear()
		plt.axes(ax3)
		plt.plot(functXPoints, functYPoints)
		plt.hlines(0, START, STOP)
		
		ax4.clear()
		plt.axes(ax4)
		plt.plot(functXPoints, functYPoints)
		plt.hlines(0, START, STOP)
		
		update(RECT_NUM)
	
	def update(r):
		global RECT_NUM
		
		RECT_NUM = int(rect_slider.val)
	
		Dx = delta_x(RECT_NUM)
		rectYPoints = function(RECT_NUM, Dx, START)
		LS, RS, Tr, MP = sumStrings(Dx, rectYPoints)
		xPoints = np.linspace(START, STOP, RECT_NUM + 1)
		midYPoints = function(RECT_NUM - 1, Dx, START + (Dx / 2.0))		#getting the y values for midpoint sum
		trapYPoints = []		
		for e in range(1, RECT_NUM + 1):		#getting y values for trapezoidal
			trapYPoints.append((rectYPoints[e - 1] + rectYPoints[e]) * 0.5)

		for i in lines:
			i.remove()
		
		lines.clear()

		plt.axes(ax1)
		lines.append(plt.hlines(rectYPoints[:-1], xPoints[:-1], xPoints[1:]))
		lines.append(plt.vlines(xPoints[:-1], 0, rectYPoints[:-1]))
		lines.append(plt.vlines(xPoints[1:], 0, rectYPoints[:-1]))
		plt.title('LHS = ' + LS) #Left Hand Sum

		plt.axes(ax2)
		lines.append(plt.hlines(rectYPoints[1:], xPoints[:-1], xPoints[1:]))
		lines.append(plt.vlines(xPoints[:-1], 0, rectYPoints[1:]))
		lines.append(plt.vlines(xPoints[1:], 0, rectYPoints[1:]))
		plt.title('RHS = ' + RS) #Right Hand Sum

		plt.axes(ax3)
		plt.title('MPS = ' + MP) #Mid Point Sum
		lines.append(plt.hlines(midYPoints, xPoints[:-1], xPoints[1:]))
		lines.append(plt.vlines(xPoints[:-1], 0, midYPoints))
		lines.append(plt.vlines(xPoints[1:], 0, midYPoints))

		plt.axes(ax4)
		plt.title('TR = ' + Tr) #Trapezoidal Rule
		lines.append(plt.hlines(trapYPoints, xPoints[:-1], xPoints[1:]))
		lines.append(plt.vlines(xPoints[:-1], 0, trapYPoints))
		lines.append(plt.vlines(xPoints[1:], 0, trapYPoints))

		fig.canvas.draw_idle()
	
	submit(FUNCTION)

	rect_slider.on_changed(update)
	text_box.on_submit(submit)

	plt.show()
	

def userInput():
	global START
	global STOP

	try:
		START = int(input('Where should sum start? '))
	except ValueError:
		print('Default starting value of %d' %START)

	try:
		STOP = int(input('Where should sum stop? '))
	except ValueError:
		print('Default stoping value of %d' %STOP)

def sumStrings(Dx, f):
	LS = str(round(LHS(Dx, f), 5))
	RS = str(round(RHS(Dx, f), 5))
	Tr = str(round(TR(Dx, f), 5))
	MP = str(round(MPS(Dx), 5))
	return LS, RS, Tr, MP

if __name__ == '__main__':
	userInput()
	draw()
	
