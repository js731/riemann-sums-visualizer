## BackGround
While acting as an employee in the Duke Math Help Room, many students came in with questions regarding different Riemann sums. I found that they grasped the concepts far quicker when they could see each sum next to eachother. This, unfortunately took lots of time and thought to hand draw the funciton multiple times, hand draw each sum, calculate each sum, and calculate the integral. 
The introduction of the Riemann Sum Visualizer allowed the students to grasp concepts more effectively while also saving everyone time.

## Functionality
Simply run the program and choose a range of x values to look at. There are defaults set in the program as global variables. Once the program is running, you can easily adjust the number of rectangles in your sum by adjusting the slider. Furthermore, you can change the function in the text box at the bottom.